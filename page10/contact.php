<?php
// DECLARATION DES VARIABLES

// DECLARATION DES FONCTIONS 
include("mvc/starter.php");

// ACTIVATION DU CODE

// CONTROLLER
formController();


if (text("view", false) == "html")
{
    // MODEL
    // CLES ET VALEURS
    addText("titrePage", "Contact");
    
    // VIEW
    include("mvc/view/header.php");
    
    include("mvc/view/section-form-contact.php");
    
    include("mvc/view/footer.php");
}
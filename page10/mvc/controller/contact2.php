<?php

// RECUPERER LES INFOS DU FORMULAIRE
// ET VERIFIER LES INFOS OBLIGATOIRES
$formRef    = checkText("formRef", "formRef obligatoire");

$email      = checkEmail("email",   "email invalide");
$nom        = checkText("nom",      "nom obligatoire");
$message    = checkText("message",  "message obligatoire");

if (countFormError() > 0)
{
    // KO
    addTextFormError($formRef);
}
else
{
    // OK
    addText($formRef, "merci pour votre message");
    
    $now = date("Y-m-d H:i:s");
    
    // TRAITER LE FORMULAIRE 
    // ENVOI D'UN MAIL
    $mailTo = "webmaster@gmail.com";
    $mailSubject = "NEW MESSAGE";
    $mailContent = 
<<<MAILCONTENT

$now
------
$email

$nom

$message
------

MAILCONTENT;

    // http://php.net/manual/fr/function.mail.php
    mail($mailTo, $mailSubject, $mailContent);
    
    // SAUVEFARDE DANS UN FICHIER
    $mois = date("ym");
    $pathLog = "mvc/model/contact/$formRef-$mois.txt";
    
    file_put_contents($pathLog, $mailContent, FILE_APPEND);
    
}

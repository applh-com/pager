<?php

// RECUPERER LES INFOS DU FORMULAIRE
// ET VERIFIER LES INFOS OBLIGATOIRES
$formRef    = checkText("formRef", "formRef obligatoire");

$email      = checkEmail("email",   "email invalide");
$password   = checkText("password",   "mot de passe obligatoire");

// ON NE METTRA LA VALEUR A true SEULEMENT SI LES INFOS SONT CORRECTES
$loginOK    = false;
$username   = "";

if (countFormError() > 0)
{
    // KO
    addTextFormError($formRef);
}
else
{
    // VERIFIER QUE LE LOGIN EST CORRECT
    $pathLogin = "mvc/model/$formRef.csv";
    if (is_file($pathLogin))
    {
        // http://php.net/manual/fr/function.file.php
        $tabLogin = file($pathLogin);
        
        // TRAITEMENT DU FORMULAIRE
        foreach($tabLogin as $ligne)
        {
            // enlève les espaces vides
            // http://php.net/manual/fr/function.file.php
            $ligne = trim($ligne);
            // découpe les infos CSV dans des variables
            // http://php.net/manual/fr/function.list.php
            list($emailUser, $passwordUser, $username) = str_getcsv($ligne);
            if (($emailUser == $email) && ($passwordUser == $password))
            {
                $loginOK = true;
            }
        }
        if ($loginOK)
        {
            // OK
            addText($formRef, "bienvenue $username");
        }
        else
        {
            // KO
            addText($formRef, "identifiants incorrects");
        }
    

    }
}
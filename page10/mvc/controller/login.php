<?php

// RECUPERER LES INFOS DU FORMULAIRE
// ET VERIFIER LES INFOS OBLIGATOIRES
$formRef    = checkText("formRef", "formRef obligatoire");

$email      = checkEmail("email",       "email invalide");
$password   = checkText("password",     "mot de passe obligatoire");

// ON NE METTRA LA VALEUR A true SEULEMENT SI LES INFOS SONT CORRECTES
$loginOK    = false;
$username   = "";

if (countFormError() > 0)
{
    // KO
    addTextFormError($formRef);
}
else
{
    list($loginOK, $username) = verifLogin($email, $password);
    
    if ($loginOK)
    {
        // OK
        addText($formRef, "bienvenue $username");
        
        // stocke les infos dans des cookies
        setcookie("email",      $email);
        setcookie("password",   $password);
        
        // redirige vers le page admin.php
        header("Location: admin.php");
    }
    else
    {
        // KO
        addText($formRef, "identifiants incorrects");
    }

}
<?php

// RECUPERER LES INFOS DU FORMULAIRE
// ET VERIFIER LES INFOS OBLIGATOIRES
$formRef    = checkText("formRef", "formRef obligatoire");

$email      = checkEmail("email",   "email invalide");

if (countFormError() > 0)
{
    // KO
    addTextFormError($formRef);
}
else
{
    // OK
    addText($formRef, "merci pour votre inscription");
    
    // TRAITEMENT DU FORMULAIRE
    $pathLog = "mvc/model/$formRef.csv";
    $dateInscription = date("Y-m-d H:i:s");
        
    $line =
<<<CODECSV
$email,$dateInscription

CODECSV;
    
    // AJOUTE UNE LIGNE AU  FICHIER CSV
    file_put_contents($pathLog, $line, FILE_APPEND);

}
<?php
// UN PEU DE SECURITE
$userLevel = $GLOBALS["userLevel"];
// LE CONTROLLER NE PEUT ETRE ACTIVE QUE PAR UN ADMIN
if ($userLevel >= 9)
{
    $fileName = getInput("fileName");
    $content  = getInput("content");
    
    // UN PEU DE SECURITE
    $fileName = basename($fileName);
    $filePath = "mvc/model/contact/$fileName";
    
    $content = strip_tags($content);
    
    if (is_file($filePath))
    {
        file_put_contents($filePath, "$content\n");
    }    
}

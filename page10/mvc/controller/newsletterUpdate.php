<?php
// UN PEU DE SECURITE
$userLevel = $GLOBALS["userLevel"];
// LE CONTROLLER NE PEUT ETRE ACTIVE QUE PAR UN ADMIN
if ($userLevel >= 9)
{

    // les espaces vides ont été enlevés au début et à la fin
    $content = getInput("content");
    // UN PEU DE SECURITE
    $content = strip_tags($content);
    
    $filePath = "mvc/model/newsletter.csv";
    // ajoute un retour à la ligne pour la prochaine inscription
    file_put_contents($filePath, "$content\n");
}
<?php

// RECUPERER LES INFOS
$email   = getInput("email");
$nom     = getInput("nom");
$message = getInput("message");

$tabErreur = [];
if ($email == "")
{
    $tabErreur[] = "EMAIL OBLIGATOIRE";
}
else 
{
    if ($email != filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        $tabErreur[] = "EMAIL INVALIDE";
    }
}

if ($nom == "")
{
    $tabErreur[] = "NOM OBLIGATOIRE";
}

if ($message == "")
{
    $tabErreur[] = "MESSAGE OBLIGATOIRE";
}

if (count($tabErreur) == 0)
{
    // TOUTES LES INFOS OBLIGATOIRES SONT PRESENTES
    addText("contact", "MERCI POUR VOTRE MESSAGE");
    
    // ENVOI D'UN MAIL
    $mailTo = "webmaster@gmail.com";
    $mailSubject = "NEW MESSAGE";
    $mailContent = 
<<<MAILCONTENT
$email

$nom

$message

MAILCONTENT;

    // http://php.net/manual/fr/function.mail.php
    mail($mailTo, $mailSubject, $mailContent);
}
else
{
    $feedback = implode($tabErreur, "<br>");
    addText("contact", $feedback);
}
<?php

// ICI ON METTRA LE CODE COMMUN A TOUTES LES PAGES

// DEFINITION DES VARIABLES GLOBALES
$GLOBALS["userLevel"] = 0;

// DEFINITION DES FONCTIONS

function startLog ()
{
    $GLOBALS["startLog"] = microtime(true);
}

// START NOW
startLog();

function endLog ()
{
    $GLOBALS["endLog"] = microtime(true);
    
    $delta = intval(1000 * ($GLOBALS["endLog"] - $GLOBALS["startLog"]));
    $memory = memory_get_peak_usage(true);
    echo 
<<<CODEHTML

<!--
delta   = $delta (ms)
memory  = $memory (bytes)
-->

CODEHTML;

}

// AJOUTER UNE CLE ET VALEUR
function addText ($key, $value)
{
    if (!isset($GLOBALS["dico"]))
    {
        $GLOBALS["dico"] = [];
    }
    $GLOBALS["dico"]["$key"] = $value;
}

// AFFICHER UNE VALEUR AVEC SA CLE
function text ($key, $echo = true)
{
    $result = "";
    if (isset($GLOBALS["dico"]))
    {
        if (isset($GLOBALS["dico"]["$key"]))
        {
            $result = $GLOBALS["dico"]["$key"];
        }
    }
    
    if ($echo == true) 
    {
        // AFFICHER LE CONTENU
        echo $result;
    }
    
    return $result;
}

// FORMULAIRES
function addFormError ($name, $error)
{
    if (!isset($GLOBALS["tabFormError"]))
    {
        $GLOBALS["tabFormError"] = [];
    }
    $GLOBALS["tabFormError"]["$name"] = $error;
}

function countFormError ()
{
    $result = 0;
    
    if (isset($GLOBALS["tabFormError"]))
    {
        $result = count($GLOBALS["tabFormError"]);
    }
    return $result;
}

function addTextFormError ($key)
{
    $message = "";
    if (isset($GLOBALS["tabFormError"]))
    {
        $message = implode($GLOBALS["tabFormError"], "<br>");    
    }
    
    addText($key, $message);
}

// RECUPERER LES INFOS DES FORMULAIRES
function getInput ($name)
{
    // DEFAULT VALUE
    $result = "";
    if (isset($_REQUEST["$name"]))
    {
        // IF THE REQUEST HAS THE DATA WITH $name
        $result = $_REQUEST["$name"];
    }
    elseif (isset($_COOKIE["$name"]))
    {
        // IF THE COOKIES HAS THE DATA WITH $name
        $result = $_COOKIE["$name"];
    }
    // http://php.net/manual/fr/function.trim.php
    return trim($result);
}


function checkText ($name, $error)
{
    $value = getInput($name);
    if ($value == "")
    {
        addFormError($name, $error);
    }
    
    // addText to show value in form
    addText($name, $value);
    
    return $value;
}

function checkEmail ($name, $error)
{
    $value = getInput($name);
    if ($value == "")
    {
        addFormError($name, $error);
    }
    elseif ($value != filter_var($value, FILTER_VALIDATE_EMAIL))
    {
        addFormError($name, $error);
    }

    // addText to show value in form
    addText($name, $value);
    
    return $value;
}

// CHARGE LE CONTROLLER DU FORMULAIRE
function formController ()
{
    // SETUP TO MANAGE AJAX
    addText("view", "html");
    
    // CHAQUE FORMULAIRE DOIT AVOIR UN CHAMP
    // <input type="hidden" name="formRef" value="newsletter">
    $formRef = getInput("formRef");
    
    // WARNING: SECURITY
    // REMOVE SOME HACK WITH ../ THAT WOULD NAVIGATE BETWEEN FOLDERS
    $formRef = basename($formRef);
    // formRef value can only have letters and numbers separayed with - or _
    $formRef = preg_replace("/[^a-zA-Z0-9-_]/", "", $formRef);
    
    if ($formRef != "")
    {
        $pathController = "mvc/controller/$formRef.php";
        // SI IL EXISTE UN FICHIER AVEC CE NOM
        if (is_file($pathController))
        {
            // CHARGE LE CODE POUR GERER LE FORMULAIRE
            include($pathController);
        }
    }
    
}

// VERIFIER SI $email et $password CORRESPONDENT A UN USER
function verifLogin ($email, $password)
{
    $loginOK    = false;
    $username   = "";
    
    // VERIFIER QUE LE LOGIN EST CORRECT
    $pathLogin = "mvc/model/login.csv";
    if (is_file($pathLogin))
    {
        // http://php.net/manual/fr/function.file.php
        $tabLogin = file($pathLogin);
        
        // TRAITEMENT DU FORMULAIRE
        foreach($tabLogin as $ligne)
        {
            // enlève les espaces vides
            // http://php.net/manual/fr/function.file.php
            $ligne = trim($ligne);
            // découpe les infos CSV dans des variables
            // http://php.net/manual/fr/function.list.php
            list($emailUser, $passwordUser, $username) = str_getcsv($ligne);
            
            if (($emailUser == $email) && ($passwordUser == $password))
            {
                $loginOK = true;
            }
        }

    }
    
    return [$loginOK, $username];
}

// CHARGE LE CONTROLEUR POUR ACCES
function accessController ($nomPage)
{
    $email      = getInput("email");
    $password   = getInput("password");
    
    list($loginOK, $username) = verifLogin($email, $password);
    
    if ($loginOK == false)
    {
        // REDIRIGER VERS LA PAGE DE LOGIN
        header("Location: login.php");
        // STOPPE LE PROGRAMME
        exit;
    }
    else
    {
        $GLOBALS["userLevel"] = 9;
    }
}
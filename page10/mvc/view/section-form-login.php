        <section id="login" class="container-fluid">
            <h2><?php text("titrePage"); ?></h2>
            <div class="row">

                <div class="col-sm-6">
                    <div>
                        <form method="POST" action="#login">
                            <input class="form-control" type="email" name="email" required placeholder="VOTRE EMAIL" value="<?php text("email"); ?>">
                            <input class="form-control" type="password" name="password" required placeholder="VOTRE MOT DE PASSE" value="<?php text("password"); ?>">
                            <button class="form-control btn btn-primary">ENVOYER LE MESSAGE</button>
                            <input type="hidden" name="formRef" value="login">
                        </form>
                    </div>
                    <div class="feedback">
                        <?php text("login"); ?>
                    </div>
                </div>

                <div class="col-sm-6">
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                        sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                        magna aliquam erat volutpat. Ut wisi enim ad minim veniam,
                        quis nostrud exerci tation ullamcorper suscipit lobortis nisl
                        ut aliquip ex ea commodo consequat. Duis autem vel eum iriure
                        dolor in hendrerit in vulputate velit esse molestie consequat,
                        vel illum dolore eu feugiat nulla facilisis at vero eros et
                        accumsan et iusto odio dignissim qui blandit praesent luptatum
                        zzril delenit augue duis dolore te feugait nulla facilisi.
                        Nam liber tempor cum soluta nobis eleifend option congue
                        nihil imperdiet doming id quod mazim placerat facer possim
                        assum. Typi non habent claritatem insitam; est usus legentis
                        in iis qui facit eorum claritatem. Investigationes
                        demonstraverunt lectores legere me lius quod ii legunt saepius.
                        Claritas est etiam processus dynamicus, qui sequitur mutationem
                        consuetudium lectorum. Mirum est notare quam littera gothica,
                        quam nunc putamus parum claram, anteposuerit litterarum formas
                        humanitatis per seacula quarta decima et quinta decima. Eodem
                        modo typi, qui nunc nobis videntur parum clari, fiant sollemnes
                        in futurum.
                    </p>
                </div>

            </div>
        </section>
        

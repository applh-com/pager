<?php

$fileMask = "mvc/model/contact/contact*.txt";
$tabFile = glob($fileMask);
foreach($tabFile as $filePath)
{
    $content    = file_get_contents($filePath);
    $fileName   = basename($filePath); 
    echo
<<<CODEHTML
<form method="POST">
    <input type="hidden" name="formRef" value="contactUpdate"/>
    <input class="form-control" type="text" name="fileName" value="$fileName" required/>
    <textarea class="form-control" rows="20" name="content">$content</textarea>
    <input class="form-control" type="submit" value="ENREGISTRER"/>
</form>

CODEHTML;

}


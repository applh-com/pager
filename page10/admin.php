<?php
// DECLARATION DES VARIABLES
$nomPage = "admin";
// DECLARATION DES FONCTIONS 
include("mvc/starter.php");

// ACTIVATION DU CODE

// CONTROLLER
accessController($nomPage);
formController();

// MODEL
// CLES ET VALEURS
addText("titrePage", "Admin");

// VIEW
include("mvc/view/header.php");

include("mvc/view/section-form-admin.php");

include("mvc/view/footer.php");

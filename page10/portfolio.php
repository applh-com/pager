<?php
// DECLARATION DES VARIABLES

// DECLARATION DES FONCTIONS 
include("mvc/starter.php");

// ACTIVATION DU CODE

// CONTROLLER
formController();

// MODEL
// CLES ET VALEURS
addText("titrePage", "Portfolio");

// VIEW
include("mvc/view/header.php");

include("mvc/view/section1.php");
include("mvc/view/section2.php");
include("mvc/view/section3.php");
include("mvc/view/section4.php");

include("mvc/view/footer.php");

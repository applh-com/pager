/* ICI ON METTRA LE CODE JAVASCRIPT POUR LE SITE */

var One = {};
One.basename = function (path, suffix) {
  //  discuss at: http://phpjs.org/functions/basename/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Ash Searle (http://hexmen.com/blog/)
  // improved by: Lincoln Ramsay
  // improved by: djmix
  // improved by: Dmitry Gorelenkov
  //   example 1: basename('/www/site/home.htm', '.htm');
  //   returns 1: 'home'
  //   example 2: basename('ecra.php?p=1');
  //   returns 2: 'ecra.php?p=1'
  //   example 3: basename('/some/path/');
  //   returns 3: 'path'
  //   example 4: basename('/some/path_ext.ext/','.ext');
  //   returns 4: 'path_ext'

  var b = path;
  var lastChar = b.charAt(b.length - 1);

  if (lastChar === '/' || lastChar === '\\') {
    b = b.slice(0, -1);
  }

  b = b.replace(/^.*[\/\\]/g, '');

  if (typeof suffix === 'string' && b.substr(b.length - suffix.length) == suffix) {
    b = b.substr(0, b.length - suffix.length);
  }

  return b;
}

One.demarrage = function ()
{
    jQuery("body").append('<div id="info" style="position:fixed;bottom:0;right:0;background-color:rgba(0,0,0,.8);color:#aaaaaa;z-index:9999;">');
    jQuery(window).on("scroll", One.onScroll);
    jQuery("nav a[href^='#']").on("click", One.onClick);
    
    One.onScroll();

    // UPDATE ACTIVE MENU    
    One.pathFile = One.basename(location.pathname);
    jQuery("nav li").removeClass("active");
    jQuery("nav li a[href='" + One.pathFile + "']").parent("li").addClass("active");
    
    // AJAX FORMS
    jQuery("form.ajax").on("submit", One.submitAjax);
    var formRef     = jQuery("form.ajax").each(One.setupAjaxForm);
}

One.setupAjaxForm = function ()
{
    var $input = jQuery(this).find("input[name=formRef]");
    if ($input.length > 0) 
    {
      $input.val($input.val() + "Ajax");
    }
}

One.submitAjax = function (event)
{
    // BLOACK NORMAL FORM PROCESSING 
    event.preventDefault();
    
    var requeteGet  = jQuery(this).serialize();
    
    jQuery(this).find(".feedback").load(location.href + "?" + requeteGet);
}

One.onScroll = function ()
{
    var top         = parseInt(jQuery(window).scrollTop());
    var height      = parseInt(jQuery("body").height());
    var pourcentage = (100 * top / height);

    var label       =  top + "/" + height + " (" + parseInt(pourcentage) + "%)"; 
    var msg         = "scroll ";
    msg             += label;
    
    jQuery("#info").html(msg);
    
}

One.onClick = function (event)
{
    event.preventDefault();

    var idTag = jQuery(this).attr("href");
    var yTag  = jQuery(idTag).position().top;
    //alert(idTag + ":" + yTag);
    
    // AJOUTER html AU SELECTEUR POUR FIREFOX
    jQuery("html, body").animate({scrollTop: yTag}, {duration:"slow", easing: "linear"});
    
}

jQuery(One.demarrage);            

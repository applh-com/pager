#!/bin/sh

# texte pour le log
text=`git status -s`
now=`date +"%m%d-%T"`

# ajoute tous les nouveaux fichiers
git add -A 

log="$now\n$text"
if [ -n "$*" ]
then
    log="$@"
fi

# enregistre toutes les modifications
# le message est composé de plusieurs infos
echo "$log" | git commit -a -F- 

# synchronise avec le repo
git push


/* ICI ON METTRA LE CODE JAVASCRIPT POUR LE SITE */

var One = {};
One.demarrage = function ()
{
    jQuery("body").append('<div id="info" style="position:fixed;bottom:0;right:0;background-color:rgba(0,0,0,.8);color:#aaaaaa;z-index:9999;">');
    jQuery(window).on("scroll", One.onScroll);
    jQuery("nav a[href^='#']").on("click", One.onClick);
    
    One.onScroll();
}

One.onScroll = function ()
{
    var top         = parseInt(jQuery(window).scrollTop());
    var height      = parseInt(jQuery("body").height());
    var pourcentage = (100 * top / height);

    var label       =  top + "/" + height + " (" + parseInt(pourcentage) + "%)"; 
    var msg         = "scroll ";
    msg             += label;
    
    jQuery("#info").html(msg);
    
}

One.onClick = function (event)
{
    event.preventDefault();
    var idTag = jQuery(this).attr("href");
    var yTag  = jQuery(idTag).position().top;
    //alert(idTag + ":" + yTag);
    
    // AJOUTER html AU SELECTEUR POUR FIREFOX
    jQuery("html, body").animate({scrollTop: yTag}, {duration:"slow", easing: "linear"});
    
}

jQuery(One.demarrage);            

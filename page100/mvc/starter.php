<?php

// ICI ON METTRA LE CODE COMMUN A TOUTES LES PAGES

// AJOUTER UNE CLE ET VALEUR
function addText ($key, $value)
{
    if (!isset($GLOBALS["dico"]))
    {
        $GLOBALS["dico"] = [];
    }
    $GLOBALS["dico"]["$key"] = $value;
}

// AFFICHER UNE VALEUR AVEC SA CLE
function text ($key)
{
    $result = "";
    if (isset($GLOBALS["dico"]))
    {
        if (isset($GLOBALS["dico"]["$key"]))
        {
            $result = $GLOBALS["dico"]["$key"];
        }
    }
    // AFFICHER LE CONTENU
    echo $result;
}

// FORMULAIRES

// RECUPERER LES INFOS DES FORMULAIRES
function getInput ($name)
{
    // DEFAULT VALUE
    $result = "";
    if (isset($_REQUEST["$name"]))
    {
        // IF THE REQUEST HAS THE DATA WITH $name
        $result = $_REQUEST["$name"];
    }
    return $result;
}

// CHARGE LE CONTROLLER DU FORMULAIRE
function formController ()
{
    // CHAQUE FORMULAIRE DOIT AVOIR UN CHAMP
    // <input type="hidden" name="formRef" value="newsletter">
    $formRef = getInput("formRef");
    
    // WARNING: SECURITY
    // REMOVE SOME HACK WITH ../ THAT WOULD NAVIGATE BETWEEN FOLDERS
    $formRef = basename($formRef);
    // formRef value can only have letters and numbers separayed with - or _
    $formRef = preg_replace("/[^a-zA-Z0-9-_]/", "", $formRef);
    
    if ($formRef != "")
    {
        $pathController = "mvc/controller/$formRef.php";
        // SI IL EXISTE UN FICHIER AVEC CE NOM
        if (is_file($pathController))
        {
            // CHARGE LE CODE POUR GERER LE FORMULAIRE
            include($pathController);
        }
    }
}
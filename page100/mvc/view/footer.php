        <footer id="footer" class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <p>&copy; <?php echo date("Y"); ?> - tous droits réservés - <a href="credits.php">crédits</a> - <a href="mentions-legales.php">mentions légales</a></p>
                </div>
            </div>
        </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="media/js/jquery-2.1.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="media/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="media/js/site.js">
    </script>
    
    <script type="text/javascript">

        /* ICI ON METTRA LE CODE JAVASCRIPT POUR LA PAGE */
        
    </script>
  </body>
</html>
<?php

// TRAITEMENT DU FORMULAIRE DE NEWSLETTER
$email = getInput("email");
// SECURITY
// EMAIL MUST BE NOT EMPTY
if ($email != "")
{
    // SECURITY
    // EMAIL MUST BE VALID
    if ($email == filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        $pathLog = "mvc/model/newsletter.csv";
        $dateInscription = date("Y-m-d H:i:s");
        
        $line =
<<<CODECSV
$email,$dateInscription

CODECSV;
    
        // AJOUTE UNE LIGNE AU  FICHIER CSV
        file_put_contents($pathLog, $line, FILE_APPEND);
        
        $feedback = 
<<<CODEHTML
<p class="bg-success">MERCI DE VOTRE INSCRIPTION $email</p>
CODEHTML;
    
        addText("newsletter", $feedback);
        
    }
    else
    {
        // ERROR MESSAGE
        $feedback = 
<<<CODEHTML
<p class="bg-warning">EMAIL INVALIDE ($email)</p>
CODEHTML;

        addText("newsletter", $feedback);
    
    }
    
}
else
{
    // ERROR MESSAGE
    $feedback = 
<<<CODEHTML
<p class="bg-warning">INFORMATION OBLIGATOIRE $email</p>
CODEHTML;

    addText("newsletter", $feedback);
    
}